# Ntuplizer for the ttH, H→ττ Analysis

## Setup

In your CMSSW area, use

    mkdir -p TTHTauTau
    git clone https://github.com/matz-e/TTHTauTau-Analysis.git TTHTauTau/Analysis
